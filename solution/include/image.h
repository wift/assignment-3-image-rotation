#ifndef IMAGE_H
#define IMAGE_H

#include  <stdint.h>
#include <stdlib.h>

struct __attribute__((packed)) pixel { 
    uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel* data;
};

struct image new_image(uint64_t hieght, uint64_t width);
void free_image(struct image* img);
#endif
