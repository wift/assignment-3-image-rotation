#pragma once
#ifndef FILE_H
#define FILE_H

#include <stdio.h>

enum open_status  {
  OPEN_OK = 0,
  OPEN_FAILED
};


enum close_status  {
  CLOSE_OK = 0,
  CLOSE_FAILED
};



enum open_status f_open( FILE** file, const char* path, const char* mode);
enum close_status f_close(FILE** file);

extern char const* open_status_msgs[];
extern char const* close_status_msgs[];
#endif
