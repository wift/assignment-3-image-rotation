#pragma once
#ifndef UTILS_H
#define UTILS_H

#define check_error(err_type, fun)\
{\
enum err_type err = fun;\
if(err != 0)\
    return err;\
}

#define handle_error(err_type, fun)\
{\
    enum err_type err = fun;\
    if(err != 0) {\
        fprintf(stdout, "%s" ,err_type##_msgs[err]); \
        return err;\
    }\
}

// #define check_error(err_type, fun)
// for(enum err_type err = fun; err != 0; ) return err;

#endif
