#pragma once
#ifndef BMP_SERIALIZER_H
#define BMP_SERIALIZER_H

#include "bmp.h"
#include  "image.h"
#include <stdio.h>

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR
  /* коды других ошибок  */
};



enum write_status to_bmp(FILE* out, struct image const* img );

extern char const* write_status_msgs[];

#endif
