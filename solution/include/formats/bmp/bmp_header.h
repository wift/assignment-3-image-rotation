#ifndef BMP_HEADER_H
#define BMP_HEADER_H

#define BF_TYPE 0x4D42
#define BI_BIT_COUNT 24
#define BI_PLANES 1
#define BI_X_PELS_PER_METER 2834
#define BI_Y_PELS_PER_METER 2834
#define BI_SIZE 40
#define PADDING 4
#include  <stdint.h>

struct __attribute__((packed)) bmp_header
{
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t biClrImportant;
};
#endif
