#ifndef BMP_DESERIALIZER_H
#define BMP_DESERIALIZER_H

#include "bmp.h"
#include  "image.h"
#include <stdio.h>

enum read_status  {
  READ_OK = 0,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_ERROR_WIHLE_READING,
  READ_INVALID_IMGAGE
  /* коды других ошибок  */
};



enum read_status from_bmp( FILE* in, struct image* img );

extern char const* read_status_msgs[];

#endif
