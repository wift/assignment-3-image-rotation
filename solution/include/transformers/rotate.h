#pragma once
#ifndef ROTATE_H
#define ROTATE_H

#include  "image.h"

enum rotate_status{
    ROTATE_OK = 0,
    ROTATE_INVALID_ANGLE,
    ROTATE_FAILED
};



enum rotate_status rotate( struct image* source, int64_t angle);
extern char const* rotate_status_msgs[];
#endif
