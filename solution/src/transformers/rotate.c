#include "image.h"
#include "transformers/rotate.h"
#include "utils.h"
#define TWO_PI 360

char const *rotate_status_msgs[] = {
    [ROTATE_OK] = "OK",
    [ROTATE_INVALID_ANGLE] = "Invalid angle value",
    [ROTATE_FAILED] = "Error while rotating image",
    
};

static enum rotate_status validate_angle(int angle) {
    if(abs(angle) % 90 != 0) {
        return ROTATE_FAILED;
    }
    return ROTATE_OK;
}

static int64_t convert_angle(int64_t angle) {
    return ((angle%TWO_PI)+TWO_PI)%TWO_PI;
}

enum rotate_status rotate( struct image* source, int64_t angle ) {
    angle = convert_angle(-angle);
    check_error(rotate_status, validate_angle(angle))
    struct image img;
    
    for(int64_t a = 0; a < angle; a+=90) {
        img = new_image(source->width, source->height);
        for(size_t y = 0; y < source->width; y++) {
            for(size_t x = 0; x < source->height; x++) {
                img.data[y * img.width + source->height - 1 - x] = source->data[x*source->width + y];
            }
        }
        free_image(source);
        *source = img;
    }

    return ROTATE_OK;
}
