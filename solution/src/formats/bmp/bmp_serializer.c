#include "formats/bmp/bmp_serializer.h"
#include "formats/bmp/bmp_header.h"
#include "utils.h"
#define PADDING 4

char const *write_status_msgs[] = {
    [WRITE_OK] = "OK",
    [WRITE_ERROR] = "Error while writing in output file",
};

static size_t getOffset(struct image const* img, size_t y) {
    return (img->width * y);
}

static size_t calc_padding(struct image const* img) {
    return (PADDING - ((img->width * sizeof(struct pixel)) % PADDING)) % PADDING;
}

static int addPadding(struct image const* img, FILE* out) {
    uint8_t zeros[PADDING] = {0,0,0,0};
    return fwrite(zeros, calc_padding(img), 1, out) < 1;
}

static enum write_status write_image(FILE* out, struct image const* img) {
    for(size_t y = 0; y < img->height; y++) {
        if(!fwrite(img->data + getOffset(img, y), sizeof(struct pixel) * img->width, 1, out)) {
            return WRITE_ERROR;
        }
        if(addPadding(img, out)) {
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

static size_t calc_img_size(struct image const* img) {
    return img->height * (calc_padding(img) + sizeof(struct pixel) * img->width);
}

static struct bmp_header new_bmp_header(struct image const* img) {
    return (struct bmp_header){
		.bfType = BF_TYPE,
		.bfileSize= sizeof(struct bmp_header) + calc_img_size(img),
		.bOffBits = sizeof (struct bmp_header),
		.biSize = BI_SIZE,
		.biWidth = img->width,
		.biHeight = img->height,
		.biPlanes = BI_PLANES,
		.biBitCount = BI_BIT_COUNT,
		.biCompression = 0,
		.biSizeImage = calc_img_size(img),
		.biXPelsPerMeter = BI_X_PELS_PER_METER,
		.biYPelsPerMeter = BI_Y_PELS_PER_METER,
		.biClrUsed = 0,
		.biClrImportant = 0,
	};
}

static enum write_status write_header(FILE* out, struct image const* img) {
    struct bmp_header header = new_bmp_header(img);
    if(!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR;
    }
    return WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img ) {
    check_error(write_status, write_header(out, img))
    check_error(write_status, write_image(out, img))
    return WRITE_OK;
}

