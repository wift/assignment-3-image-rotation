#include "formats/bmp/bmp_deserializer.h"
#include "formats/bmp/bmp_header.h"
#include "utils.h"
#define PADDING 4

char const *read_status_msgs[] = {
    [READ_OK] = "OK",
    [READ_INVALID_SIGNATURE] = "Invalid signature in input file",
    [READ_INVALID_BITS] = "Invalid bits in input file",
    [READ_INVALID_HEADER] = "Invalid header in input file",
    [READ_ERROR_WIHLE_READING] = "Error while reading input file",
    [READ_INVALID_IMGAGE] = "Invalid imge in input file",
    
};

static size_t getOffset(struct image const* img, size_t y) {
    return (img->width * y);
}

static int passPadding(struct image const* img, FILE* in) {
    return fseek(in, (long)(PADDING - (sizeof(struct pixel)*img->width % PADDING)) % PADDING, SEEK_CUR);
}

static enum read_status read_image(FILE* in, struct image* img) {
    for(size_t y = 0; y < img->height; y++) {
        if(!fread(img->data + getOffset(img, y), sizeof(struct pixel), img->width, in)) {
            return READ_ERROR_WIHLE_READING;
        }
        if(passPadding(img, in)) {
            return READ_ERROR_WIHLE_READING;
        }
    }
    return READ_OK;
}

static enum read_status validate(struct bmp_header const* header) {
    if(header->bfType != BF_TYPE) {
        return READ_INVALID_SIGNATURE;
    }
    if(header->bfReserved) {
        return READ_INVALID_HEADER;
    }
    if(header->bOffBits < sizeof(struct bmp_header)) {
        return READ_INVALID_HEADER;
    }
    if(header->biWidth * header->biHeight == 0) {
        return READ_INVALID_HEADER;
    }
    if(header->biPlanes != BI_PLANES) {
        return READ_INVALID_HEADER;
    }
    if(header->biBitCount != BI_BIT_COUNT) {
        return READ_INVALID_BITS;
    }
    if(header->biCompression != 0) {
        return READ_INVALID_HEADER;
    }
    if(header->biSizeImage == 0) {
        return READ_INVALID_HEADER;
    }
    return READ_OK;
}



enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if(!fread(&header, sizeof(struct bmp_header), 1, in)) {
        return READ_ERROR_WIHLE_READING;
    }
    check_error(read_status, validate(&header))

    *img = new_image(header.biHeight, header.biWidth);
    fseek(in, header.bOffBits, SEEK_SET);
    check_error(read_status, read_image(in, img))
    return READ_OK;
}
