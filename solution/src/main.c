#include <stdio.h>

#include "file.h"
#include "formats/bmp/bmp_deserializer.h"
#include "image.h"
#include "transformers/rotate.h"
#include "utils.h"

#include "formats/bmp/bmp_serializer.h"

int main( int argc, char** argv ) {
    if(argc < 4) {
        fprintf(stderr, "too few arguments");
        return 1;
    }
    FILE *in, *out;
    handle_error(open_status, f_open(&in, argv[1], "rb"))
    struct image img;
    handle_error(read_status, from_bmp(in, &img))
    handle_error(close_status, f_close(&in))
    handle_error(rotate_status, rotate(&img, (int64_t)(atoi(argv[3]))))

    handle_error(open_status, f_open(&out, argv[2], "wb"))
    handle_error(write_status, to_bmp(out, &img))
    handle_error(close_status, f_close(&out))
    free_image(&img);
    return 0;
}
