#include "file.h"

char const *open_status_msgs[] = {
    [CLOSE_OK] = "OK",
    [CLOSE_FAILED] = "Can't open file",
};

char const *close_status_msgs[] = {
    [CLOSE_OK] = "OK",
    [CLOSE_FAILED] = "Can't close file",
};

enum open_status f_open( FILE** file, const char* path, const char* mode) {
    *file = fopen(path, mode);
    if(!(*file) || ferror(*file)) {
        return OPEN_FAILED;
    }
    return OPEN_OK;
}

enum close_status f_close(FILE** file) {
    if(fclose(*file)) {
        return CLOSE_FAILED;
    }
    *file = NULL;
    return CLOSE_OK;
}
