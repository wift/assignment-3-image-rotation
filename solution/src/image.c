#include "image.h"
#include <stdio.h>

struct image new_image(uint64_t hieght, uint64_t width) {
    struct image img = {.height = hieght, .width = width,
     .data = malloc(sizeof(struct pixel) * hieght *width)};
    if(!img.data) {
        fprintf(stderr, "out of memory");
        exit(1);
    }
    return img;
}

inline void free_image(struct image* img) {
    if(img->data) {
        free(img->data);
        img->data = NULL;
    }
}
